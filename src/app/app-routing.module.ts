import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { NewFilmComponent } from './new-film/new-film.component';
import { ManageFilmsComponent } from './manage-films/manage-films.component';
import { NotFoundComponent } from './not-found';

const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'new-films', component: NewFilmComponent},
  {path: 'manage-films', component: ManageFilmsComponent},
  {path: '**', component: NotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
