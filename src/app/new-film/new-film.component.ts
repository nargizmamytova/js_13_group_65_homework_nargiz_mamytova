import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FilmService } from '../shared/film-service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-new-film',
  templateUrl: './new-film.component.html',
  styleUrls: ['./new-film.component.css']
})
export class NewFilmComponent implements OnInit {
  @ViewChild('filmName')filmName!: ElementRef;
  @ViewChild('filmDate')filmDate!: ElementRef;
  @ViewChild('filmUrl')filmUrl!: ElementRef;
  @ViewChild('filmInfo')filmInfo!: ElementRef;

  constructor( private filmService: FilmService, private router: Router) { }
  loading: boolean = false;
  ngOnInit(): void {}

  createFilm() {
    const filmName: string = this.filmName.nativeElement.value;
    const filmUrl: string = this.filmUrl.nativeElement.value;
    const filmDate: number = this.filmDate.nativeElement.value;
    const filmInfo: string = this.filmInfo.nativeElement.value;
    const id = '';
    const body = {id, filmName, filmUrl, filmDate, filmInfo};
    this.filmService.addFilms(body);
    this.filmService.fetchFilms();
    this.loading = true;
    void this.router.navigate(['/']);
  }
}
