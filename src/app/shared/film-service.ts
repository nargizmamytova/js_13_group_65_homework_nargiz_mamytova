import { Film } from './film-model';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import { Injectable } from '@angular/core';

@Injectable()

export class FilmService{
  filmsChange = new Subject<Film[]>();
  filmsFetching = new Subject<boolean>();
  private films: Film[] = [];

  constructor(private http: HttpClient) { }
  getFilms(){
    return this.films.slice()
  }
  addFilms(body: Film){
    this.films.push(body);
    this.filmsChange.next(this.films);
    this.http.post('https://plovo-bb6ec-default-rtdb.firebaseio.com/films.json', body).subscribe();
  }
  fetchFilms(){
    this.filmsFetching.next(true);
    this.http.get<{[id: string]: Film}>('https://plovo-bb6ec-default-rtdb.firebaseio.com/films.json')
      .pipe(map(result => {
        return Object.keys(result).map(id => {
          const filmData = result[id]
          return new Film(
            id, filmData.filmName, filmData.filmUrl,
            filmData.filmDate, filmData.filmInfo
          )
        })
      }))
      .subscribe(films => {
        this.films = films;
        this.filmsChange.next(this.films.slice());
        this.filmsFetching.next(false)
      }, error => {
        this.filmsFetching.next(false)
      })
  }
  deleteFilms(filmsId: string){
    this.http.delete(`https://plovo-bb6ec-default-rtdb.firebaseio.com/films/${filmsId}.json`).subscribe()
  }

}

