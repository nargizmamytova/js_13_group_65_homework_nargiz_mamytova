export class Film{
  constructor(
    public id: string,
    public filmName: string,
    public filmUrl: string,
    public filmDate: number,
    public filmInfo: string
  ) {}
}
