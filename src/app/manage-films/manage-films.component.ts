import { Component, OnDestroy, OnInit } from '@angular/core';
import { Film } from '../shared/film-model';
import { FilmService } from '../shared/film-service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-manage-films',
  templateUrl: './manage-films.component.html',
  styleUrls: ['./manage-films.component.css']
})
export class ManageFilmsComponent implements OnInit{
  films!: Film[];
  loading: boolean = false;
  constructor(private filmService: FilmService, private router: Router) {
  }
  ngOnInit(): void {
    this.films = this.filmService.getFilms();
  }

  onDelete(filmId: string) {
    this.filmService.deleteFilms(filmId);
    this.filmService.fetchFilms();
    this.loading = true;
    void this.router.navigate(['/']);
  }
}
