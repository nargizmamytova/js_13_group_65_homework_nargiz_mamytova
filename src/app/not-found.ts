import { Component } from '@angular/core';

@Component({
  selector: 'app-not-found',
  template: `<h1>We can't find this page</h1>`
})
export class NotFoundComponent{

}
