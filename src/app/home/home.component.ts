import { Component, OnDestroy, OnInit } from '@angular/core';
import { Film } from '../shared/film-model';
import { Subscription } from 'rxjs';
import { FilmService } from '../shared/film-service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, OnDestroy {
  films!: Film[];
  filmsChangeSubscription!: Subscription;
  filmsFetchSubscription!: Subscription;
  loading: boolean = false;
  constructor(private filmService: FilmService) { }

  ngOnInit(): void {
    this.films = this.filmService.getFilms();
    this.filmsChangeSubscription = this.filmService.filmsChange.subscribe((films: Film[]) => {
      this.films = films;
    });
    this.filmsFetchSubscription = this.filmService.filmsFetching.subscribe((isFetching: boolean) => {
      this.loading = isFetching;
    });
    this.filmService.fetchFilms();
  }
  ngOnDestroy() {
    this.filmsChangeSubscription.unsubscribe();
    this.filmsFetchSubscription.unsubscribe();
  }

}
